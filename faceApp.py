# -*- coding: utf-8 -*-
import datetime
from matplotlib.pyplot import imshow
import math
from PIL import Image
from inception_blocks import *
from utils import *
import pandas as pd
from numpy import genfromtxt
import os
from keras import backend as K
import sys
import time
import numpy as np
import tensorflow as tf
import cv2
from flask import Flask, jsonify, request
app = Flask(__name__)

K.set_image_data_format('channels_first')


recognitionModel = RecognitionModel(input_shape=(3, 96, 96))
print("Total Params:", recognitionModel.count_params())


def triplet_loss(y_true, y_pred, alpha=0.2):

    anchor, positive, negative = y_pred[0], y_pred[1], y_pred[2]
    # distance between the anchor and the positive
    pos_dist = tf.reduce_sum(tf.square(tf.subtract(anchor, positive)), axis=-1)
    # distance between the anchor and the negative
    neg_dist = tf.reduce_sum(tf.square(tf.subtract(anchor, negative)), axis=-1)
    # basic loss
    basic_loss = (pos_dist - neg_dist) + alpha
    # loss
    loss = tf.reduce_sum(tf.maximum(basic_loss, 0.0))
    return loss


recognitionModel.compile(
    optimizer='adam',
    loss=triplet_loss,
    metrics=['accuracy'])
load_weights_from_FaceNet(recognitionModel)
print("Finish loading weight")


def preprocess_image(image1, image2):
    image1 = Image.open(image1)
    image2 = Image.open(image2)

    width = float(image1.size[0])
    height = float(image1.size[1])

    resize_width = float(100)
    resize_height = float(math.ceil(((height / width) * resize_width)))

    image1 = image1.resize(
        (int(resize_width),
         int(resize_height)),
        Image.ANTIALIAS)

    x = image1.size[0]
    y = image1.size[1]

    x_mid = int(round(x / 2))
    y_mid = int(round(y / 2))

    # calculating the mid point
    x_start = x_mid - 48
    y_start = y_mid - 48

    print("start point:", x_start, y_start)

    # outfile1 = "Output/"+image_name + str(index)+".jpg"
    region1 = image1.crop((x_start, y_start, x_start + 96, y_start + 96))

    # with tf.gfile.Open(outfile1, 'w') as fid:
    # 	region.save(fid, 'JPEG')

    print("Cropping ended")

    width = float(image2.size[0])
    height = float(image2.size[1])

    resize_width = float(100)
    resize_height = float(math.ceil(((height / width) * resize_width)))

    image2 = image2.resize(
        (int(resize_width),
         int(resize_height)),
        Image.ANTIALIAS)

    x = image2.size[0]
    y = image2.size[1]

    x_mid = int(round(x / 2))
    y_mid = int(round(y / 2))

    # calculating the mid point
    x_start = x_mid - 48
    y_start = y_mid - 48

    print("start point:", x_start, y_start)

    # outfile1 = "Output/"+image_name + str(index)+".jpg"
    region2 = image2.crop((x_start, y_start, x_start + 96, y_start + 96))

    # with tf.gfile.Open(outfile1, 'w') as fid:
    # 	region.save(fid, 'JPEG')

    print("Cropping ended")

    return region1, region2


def Predict(image1, image2, model):
    preimg1, preimg2 = preprocess_image(image1, image2)
    encoding1 = img_to_encoding(image1, model)
    encoding2 = img_to_encoding(image2, model)

    dist = np.linalg.norm(encoding1 - encoding2)

    if dist > 0.5:
        return dist, "Not the same person"
    else:

        return dist, "Same person"




@app.route('/login', methods=['POST'])
def upload_base64_file():
    """
        Upload image with base64 format and get car make model and year
        response
    """
    
    posted_data = request.files        
                                           
    print(posted_data)                                          
  
    print("ENTER_------------------------------------------------->")
    print(request.files['img1'])
    r = request
    # convert string of image data to uint8
    #nparr = np.fromstring(r.data, np.uint8)
    
    if data is None:
        print("No valid request body, json missing!")
        return jsonify({'error': 'No valid request body, json missing!'})
    else:
    	img_data = data['img']
    	# this method convert and save the base64 string to image
    	ans = convert_and_save(img_data)
    	return ans

def convert_and_save(b64_string1, b64_string2):

    with open("pree1.jpg", "wb") as fh:
        fh.write(base64.decodebytes(b64_string.encode()))

    with open("pree2.jpg", "wb") as fh:
        fh.write(base64.decodebytes(b64_string.encode()))

    return Predict("pree1.jpg", "pree2.jpg", recognitionModel)

 # start flask app
app.run(host="0.0.0.0", port=5000)

if __name__ == "__main__":
    print("Sleeping start")
    very = "Variable set"
    print("Sleeping end")
    app.run(debug=True)
